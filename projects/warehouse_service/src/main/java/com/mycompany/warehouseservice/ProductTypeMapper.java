package com.mycompany.warehouseservice;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.mycompany.warehouseservice.api.model.ProductTypeDto;

@Mapper
public interface ProductTypeMapper {
    public static ProductTypeMapper INSTANCE = Mappers.getMapper(ProductTypeMapper.class);

    ProductTypeDto mapTo(ProductType wp);
    ProductType mapTo(ProductTypeDto wpDto);
}