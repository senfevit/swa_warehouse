package com.mycompany.warehouseservice;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.JpaRepository;

import java.io.Serializable;
import java.util.List;

public interface WarehouseProductRepository extends JpaRepository<WarehouseProduct, Long> {
    @Query("SELECT wp FROM WarehouseProduct wp WHERE wp.productType.id = ?1 AND wp.booked = FALSE") //jde o jpa entity ne tabulky -> ne jmeno tabulky ale entity
    List<WarehouseProduct> findUnreservedByProductTypeId(Long productTypeId);
}