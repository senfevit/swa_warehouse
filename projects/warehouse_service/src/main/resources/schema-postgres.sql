DROP TABLE IF EXISTS warehouse_product_table;
DROP TABLE IF EXISTS product_type_table;

CREATE TABLE product_type_table(id SERIAL PRIMARY KEY, name VARCHAR(255));

--CREATE TABLE warehouse_product_table(id SERIAL PRIMARY KEY, weight integer, location VARCHAR(255), product_id INT REFERENCES product_type);
CREATE TABLE warehouse_product_table(
    id SERIAL PRIMARY KEY,
    weight integer,
    location VARCHAR(255),
    booked boolean,
    product_id INT REFERENCES product_type_table (id));